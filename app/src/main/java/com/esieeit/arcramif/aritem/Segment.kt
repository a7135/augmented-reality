package com.esieeit.arcramif.aritem

import com.google.ar.core.Anchor
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.ux.TransformableNode
import com.google.ar.sceneform.ux.TransformationSystem
import kotlin.math.pow
import kotlin.math.sqrt

/**
 * Class Segment
 * It's a base class for the Segment Ar with the array of points and anchors
 * And method to manipulate a segment object
 */
open class Segment(transformationSystem: TransformationSystem?) : TransformableNode(
    transformationSystem
) {
    private val anchors = ArrayList<Anchor>(2)
    private val anchorsNode = ArrayList<AnchorNode>(2)
    val points = ArrayList<Point>(2)

    /**
     * Return the anchors
     */
    fun getAnchors(): ArrayList<Anchor> {
        return this.anchors;
    }

    /**
     * Set anchors on index (switch a point anchor)
     * @param anchor
     * @param index
     */
    fun setAnchorOnIndex(anchor: Anchor,index: Int) {
        anchors.set(index,anchor);
    }

    /**
     * Add anchors to the list
     * @param anchor
     */
    fun addAnchor(anchor: Anchor) {
        anchors.add(anchor);
    }

    /**
     * Return the NodeAnchors
     */
    fun getAnchorsNode(): ArrayList<AnchorNode> {
        return this.anchorsNode;
    }

    /**
     * Add point to the list
     * @param point
     */
    fun addPoint(point: Point) {
        points.add(point);
    }

    /**
     * Set a Point in the list at the index
     * @param point
     *@param index
     */
    fun setPointOnIndex(point: Point, index: Int) {
        points.set(index,point);
    }

    /**
     * get the  point a the index
     * @param point
     *@param index
     */
    fun getPointOnIndex(point: Point, index: Int) {
        points.set(index,point);
    }

    /**
        Method that computes the relative distance between 2 placed anchors.

        NOTE : For now, it only computes the distance between the last placed anchor and the previous one.
         */
    fun measureDistanceOf2Points(): String {
        val distanceMeter = calculateDistance(
            this.points[0].anchorNode.worldPosition,
            this.points[1].anchorNode.worldPosition)
        return measureDistanceOf2Points(distanceMeter)
    }

    /**
     * Method that return the Float value of a Vector3
     * @param x
     * @param y
     * @param z
     */
    private fun calculateDistance(x: Float, y: Float, z: Float): Float{
        return sqrt(x.pow(2) + y.pow(2) + z.pow(2))
    }

    /**
     * Method to compute the disctance between two position (Vector3)
     * @param objectPose0 position 1
     * @param objectPose1 position 2
     */
    private fun calculateDistance(objectPose0: Vector3, objectPose1: Vector3): Float{
        return calculateDistance(
            objectPose0.x - objectPose1.x,
            objectPose0.y - objectPose1.y,
            objectPose0.z - objectPose1.z
        )
    }


    /**
     * Method call to get the distance in cm of a meter distance
     * @param distanceMeter
     */
    private fun measureDistanceOf2Points(distanceMeter : Float): String {
        val distanceTextCM = makeDistanceTextWithCM(distanceMeter)
        return distanceTextCM;
    }

    /**
    Method that generates the string will be displayed in the distanceCardView by using
    the computed distance between two nodes and adding "cm" at the end.

    NOTE : If the distance isn't in cm, the the value will be converted with the method changeUnit()
     *@param distanceMeter
     */
    private fun makeDistanceTextWithCM(distanceMeter: Float): String{
        val distanceCM = changeUnit(distanceMeter, "cm")
        val distanceCMFloor = "%.2f".format(distanceCM)
        return "${distanceCMFloor} cm"
    }

    /**
     * change value to unit selected
     * @param distanceMeter
     * @param unit
     */
    private fun changeUnit(distanceMeter: Float, unit: String): Float {
        return when (unit) {
            "cm" -> distanceMeter * 100
            "mm" -> distanceMeter * 1000
            else -> distanceMeter
        }
    }
}