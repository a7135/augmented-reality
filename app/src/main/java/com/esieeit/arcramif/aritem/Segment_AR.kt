package com.esieeit.arcramif.aritem

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.widget.TextView
import com.esieeit.arcramif.R
import com.google.ar.core.Pose
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.FrameTime
import com.google.ar.sceneform.Node
import com.google.ar.sceneform.math.Quaternion
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.*
import com.google.ar.sceneform.ux.ArFragment
import com.google.ar.sceneform.ux.TransformableNode
import com.google.ar.sceneform.ux.TransformationSystem
import java.util.function.Consumer
import java.util.function.Function

/**
 * Class Segment_AR is a AR item
 * Compose with 2 points (Point)
 * A middle card with the text of the distance in cm
 * and a line draw between the 2 points
 * @param context is the activity (ArMainModule)
 * @param arFragment is the Ar fragment generated in the parent view, it's use to create ArItem and set their parents scene
 * @param p1 first point
 * @param p2 second point
 * @param transformationSystem the handler of a transformable node (is selected and is moving)
 */
class Segment_AR(context: Context, var arFragment: ArFragment, var p1 : Point, var p2: Point,
                 transformationSystem: TransformationSystem?
) : Segment(transformationSystem) {
    private var context : Context? = context;
    private var distanceCardView: Node? = null;
    private var distanceCardTextView: TextView? = null;
    private var renderLine: ModelRenderable? = null;
    var midAnchorNode =AnchorNode();
    var lineBetweenPoint: Node? = null;

    /**
     * Methode wich will instanciate the semgent
     * Set the 2 points And create the 2 render : distanceCard and the line
     *
     */
    override fun onActivate() {
        points.add(p1)
        points.add(p2)
        if(distanceCardView == null) {
            setMidAnchor(arFragment)

            distanceCardView = Node()
            distanceCardView!!.setParent(midAnchorNode)
            distanceCardView!!.isEnabled = false
            ViewRenderable.builder()
                .setView(context, R.layout.distance_text_layout)
                .build()
                .thenAccept(
                    Consumer { renderable: ViewRenderable ->
                                distanceCardView!!.renderable = renderable
                        renderable.isShadowCaster=false
                        renderable.setShadowReceiver(false)
                        distanceCardTextView = renderable.view.findViewById(R.id.distanceCard)
                        distanceCardTextView!!.setText(measureDistanceOf2Points())
                        distanceCardView!!.isEnabled = true
                    })
                .exceptionally(
                    Function<Throwable, Void> { throwable: Throwable? ->
                        throw AssertionError(
                            "Could not load plane card view.",
                            throwable
                        )
                    })
        }
        if(renderLine == null){
            MaterialFactory.makeOpaqueWithColor(this.context, Color(Color.BLUE))
                .thenAccept { material: Material? ->
                    /* Then, create a rectangular prism, using ShapeFactory.makeCube() and use the difference vector
                to extend to the necessary length.  */
                    renderLine = ShapeFactory.makeCube(
                        Vector3(.01f, .01f, 0.01f),
                        Vector3.zero(), material
                    )
                    this.makeLine()
                }
        }
    }

    /**
     * Method that will
     * if one of the point is selected it's going to refresh the line and the position of the card
     * Else it's will orientate the distance card look direction in relation to the camera direction in order
     * to keep the distance card "in front" of the camera
     * @param p0
     */
    override fun onUpdate(p0: FrameTime?) {
//        super.onUpdate(p0)
        if(renderLine!=null && distanceCardView !=null && (points[0].isSelected || points[1].isSelected)) {
            refreshLineBetweenNodes()
            refreshDistanceCard()
        }
        if(distanceCardView !=null){
            val cameraPosition = scene!!.camera.worldPosition
            val cardPosition: Vector3 = distanceCardView!!.worldPosition
            val direction = Vector3.subtract(cameraPosition, cardPosition)
            val lookRotation = Quaternion.lookRotation(direction, Vector3.up())
            distanceCardView!!.worldRotation = lookRotation

        }
        return
    }

    /**
     * Method call when the Class is delete of the view
     */
    override fun onDeactivate() {
        midAnchorNode.setParent(null)
        distanceCardView?.setParent(null);
        super.onDeactivate()
    }

    /**
     * Delete saved anchor
     */
    fun deleteAnchor(){
        midAnchorNode.setParent(null)
        distanceCardView?.setParent(null);

    }

    /**
     * Method that will recalculate the difference between the 2 points
     * in order to get the good size and rotation
     */
    private fun refreshLineBetweenNodes() {
        if(lineBetweenPoint==null){
            makeLine()
        } else if(this.points[0].isSelected || this.points[1].isSelected) {
            val difference = Vector3.subtract(
                super.points[1].worldPosition,
                super.points[0].worldPosition
            )
            val directionFromTopToBottom = difference.normalized()
            val rotationFromAToB = Quaternion.lookRotation(directionFromTopToBottom, Vector3.up())
            lineBetweenPoint!!.worldPosition = Vector3.add(
                super.points[0].worldPosition,
                super.points[1].worldPosition
            ).scaled(.5f)
            renderLine = ShapeFactory.makeCube(
                Vector3(.01f, .01f,difference.length()),
                Vector3.zero(), renderLine?.material
            )
            renderLine!!.isShadowCaster=false;
            renderLine!!.isShadowReceiver=false;
            lineBetweenPoint!!.renderable = renderLine
            lineBetweenPoint!!.worldRotation = rotationFromAToB

        }
    }


    /**
    Method that sets the text in the distanceCardView to the text generated by the method makeDistanceTextWithCM().
     */
    private fun refreshDistanceCard() {
        if(this.points[0].isSelected || this.points[1].isSelected) {
            val midPosition = Vector3(
                (points[0].worldPosition.x + points[1].worldPosition.x) / 2,
            (points[0].worldPosition.y + points[1].worldPosition.y) / 2,
            (points[0].worldPosition.z + points[1].worldPosition.z) / 2)
            distanceCardView!!.worldPosition = midPosition ;
            distanceCardTextView!!.text = measureDistanceOf2Points()
        }
    }

    /**
     * Method called once to create the line between points
     */
    private fun makeLine(){
        val firstAnchorNode = super.points[0].anchorNode
        val difference = Vector3.subtract(
            super.points[1].anchorNode.worldPosition,
            firstAnchorNode.worldPosition
        )
        val directionFromTopToBottom = difference.normalized()
        val rotationFromAToB = Quaternion.lookRotation(directionFromTopToBottom, Vector3.up())
        renderLine = ShapeFactory.makeCube(
            Vector3(.01f, .01f,difference.length()),
            Vector3.zero(), renderLine?.material
        )
        renderLine!!.isShadowCaster=false;
        renderLine!!.isShadowReceiver=false;

        /* Last, set the world rotation of the node to the rotation calculated earlier and set the world position to
    the midpoint between the given points . */
        lineBetweenPoint = Node()
        lineBetweenPoint!!.setParent(midAnchorNode)
        lineBetweenPoint!!.renderable = renderLine
        lineBetweenPoint!!.worldPosition = Vector3.add(
            super.points[0].worldPosition,
            super.points[1].worldPosition
        ).scaled(.5f)
//                lineBetweenPoint!!.localScale = Vector3(.01f, .01f, difference.length())
        lineBetweenPoint!!.worldRotation = rotationFromAToB
    }


    /**
    Method that create the middle Anchor between the 2 points of the segment and
     * @param arFragment in order to set the anchor node created
     */
    fun setMidAnchor(arFragment: ArFragment){
            placeMidAnchor(getMidPose(),arFragment)
        }

    /**
     * Method to create the Pose object placed at the middle of the 2 points of the segment
     */
    fun getMidPose() : Pose{
        val firstAnchor = points[0].anchorNode
        val secondAnchor = points[1].anchorNode
        val midPosition = floatArrayOf(
            (firstAnchor.worldPosition.x + secondAnchor.worldPosition.x) / 2,
            (firstAnchor.worldPosition.y + secondAnchor.worldPosition.y) / 2,
            (firstAnchor.worldPosition.z + secondAnchor.worldPosition.z) / 2)
        val quaternion = floatArrayOf(0.0f,0.0f,0.0f,0.0f)
        val pose = Pose(midPosition, quaternion)
       return pose
    }


//
    /**
     * Method that create the middle anchor from the Pose and add it to the fragment
     * and update the distance card position
     * @param pose is a world pose use to create a anchor
     * @param arFragment
     */

    private fun placeMidAnchor(pose: Pose,
                               arFragment: ArFragment){
        val anchor = arFragment.arSceneView.session!!.createAnchor(pose)

        midAnchorNode = AnchorNode(anchor).apply {
            isSmoothed = true
            setParent(arFragment.arSceneView.scene)
        }

         distanceCardView = TransformableNode(arFragment.transformationSystem)
            .apply{
                this.rotationController.isEnabled = false
                this.scaleController.isEnabled = false
                this.translationController.isEnabled = true
                this.renderable = renderable
                setParent(midAnchorNode)
            }
    }
}