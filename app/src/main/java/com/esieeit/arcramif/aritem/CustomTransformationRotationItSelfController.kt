package com.esieeit.arcramif.aritem

import com.google.ar.sceneform.math.Quaternion
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.ux.BaseTransformableNode
import com.google.ar.sceneform.ux.BaseTransformationController
import com.google.ar.sceneform.ux.DragGesture
import com.google.ar.sceneform.ux.DragGestureRecognizer

/**
 * This class is a fonctionnal test for custom transformaion controller for Transformable node
 * @param transformableNode it's the transformable node wich will have this controller as handler
 * @param gestureRecognizer the gesture event from the user with the action realised
 */
class CustomTransformationRotationItSelfController(transformableNode: BaseTransformableNode, gestureRecognizer: DragGestureRecognizer) :
    BaseTransformationController<DragGesture>(transformableNode, gestureRecognizer) {

    // Rate that the node rotates in degrees per degree of twisting.
    var rotationRateDegrees = 0.5f

    /**
     * Check if the node is selected
     */
    public override fun canStartTransformation(gesture: DragGesture): Boolean {
        return transformableNode.isSelected
    }

    /**
     * While the node is selected it will change it related to the gesture
     * @param gesture the movement of the user
     */
    public override fun onContinueTransformation(gesture: DragGesture) {

//        var localRotation = transformableNode.localRotation
//
        //val rotationAmountX = gesture.delta.x * rotationRateDegrees
//        val rotationDeltaX = Quaternion(Vector3.up(), rotationAmountX)
//        localRotation = Quaternion.multiply(localRotation, rotationDeltaX)
        //transformableNode.setLookDirection(Vector3())
    }

    /**
     * Call at the end of the gesture
     * @param gesture last movement
     */
    public override fun onEndTransformation(gesture: DragGesture) {}
}