package com.esieeit.arcramif.aritem;

import android.app.AlertDialog
import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import com.esieeit.arcramif.R
import com.esieeit.arcramif.data.StepValue
import com.google.ar.core.Plane
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.FrameTime
import com.google.ar.sceneform.Node
import com.google.ar.sceneform.math.Quaternion
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.DpToMetersViewSizer
import com.google.ar.sceneform.rendering.ViewRenderable
import com.google.ar.sceneform.ux.ArFragment
import com.google.ar.sceneform.ux.TransformableNode

/**
 * Class ConfortZone
 * A Ar Item with based on a segment and with only a demi circle render for the moment
 * @param context the parent
 * @param stepValue the value of the current step "zone"
 * @param arFragment the ArFragment parent
 */
class ConfortZone(private val context: Context, val stepValue: StepValue,
                 val arFragment: ArFragment)
    : Node() {
    lateinit var segment_AR:Segment_AR
    lateinit var renderConfortZone: ViewRenderable
    lateinit var renderImageView: ImageView
    lateinit var previusPoint1WorldPosition : Vector3
    lateinit var previusPoint2WorldPosition : Vector3

    /**
     * The onActivate Method is the first function to be call for a Node Object
     * Our method will Create the render for this ConfortZone
     * (we create a render for each confortZone in order to change the color of each separately
     * If we set the same renderable on two distinct confortZone and we change the color of only one of them,
     * the 2 renderable will change color...)
     *
     * Then we set the deformation (scale) of the zone in order to match the data in millimeter
     *
     * Finally we set the color from the data to the drawable shape
     *
     */
    override fun onActivate() {
            if (scene == null) {
                throw IllegalStateException("Scene is null!");
            }
            ViewRenderable
                .builder()
                .setView(context, R.layout.confort_zone_layout)
                .build()
                .thenAccept{
                        renderable: ViewRenderable ->
                    renderConfortZone = renderable
                    renderConfortZone.isShadowCaster = false
                    renderConfortZone.isShadowReceiver = false
                    renderImageView = renderable.view.findViewById(R.id.confortZoneShape)

                    // Set Scale
                    val dpParMeter = 1000
                    renderConfortZone.sizer = DpToMetersViewSizer(dpParMeter)
                    val dpToMinConvert =dpParMeter/1000
                    val scaleX =  (stepValue.width/100).toFloat()
                    val scaleY =  (stepValue.heigth/50).toFloat()
                    this.localScale = Vector3(scaleX,scaleY,0f)
                    //SetColor
                    val drawable = context.resources.getDrawable(R.drawable.confort_zone_semi_circle,
                        context.theme)
                    drawable.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
                        Color.parseColor(stepValue.color), BlendModeCompat.PLUS)
                    renderImageView.setImageDrawable(drawable)
                    this.renderable = renderConfortZone;

                }
                .exceptionally {
                val builder = AlertDialog.Builder(context)
                builder.setMessage(it.message).setTitle("Error")
                val dialog = builder.create()
                dialog.show()
                return@exceptionally null
            }
    }

    /**
     * This method will set the base of the Confort Zone to a segment
     * 1: Set the Anchor in the center of the segment
     * 2: And align it in a perpendicular direction
     * 3: rotate it in order to be ON the ground
     * Finally save the last Points positions (For performance)
     * @param segmentAr the segment based on
     */
    fun setSegmentAnchors(segmentAr: Segment_AR): AnchorNode {
        this.segment_AR = segmentAr
        //Set mid anchor
        val anchor = arFragment.arSceneView.session!!.createAnchor(segmentAr.getMidPose())
        val anchorMid = AnchorNode(anchor)
        anchorMid.setParent(arFragment.arSceneView.scene)
        this.setParent(anchorMid);

        placeConfortZoneBaseOnSegment()
        return anchorMid
    }

    /**
     * Method on update to handle change of the segment
     * If there is a modification from the semgent we change the position and direction
     * of the confortZone
     * @param p0 give by the lifeCycle of the ArFrament
     */
    override fun onUpdate(p0: FrameTime?) {
        super.onUpdate(p0)
        //If point haven't change
        if( previusPoint1WorldPosition.equals(segment_AR.p1.worldPosition) && previusPoint2WorldPosition.equals(segment_AR.p2.worldPosition)){
            return
        }
        placeConfortZoneBaseOnSegment()
    }

    /**
     * Method for confort Zone with semgent step base
     * It will set the confort zone to the mid anchor of the segment base
     * And will orientate the confort zone in order to be perpendicular to the segment
     */
    fun placeConfortZoneBaseOnSegment(){
        //Update Mid position
        val midPosition = Vector3(
            (segment_AR.p1.worldPosition.x + segment_AR.p2.worldPosition.x) / 2,
            (segment_AR.p1.worldPosition.y + segment_AR.p2.worldPosition.y) / 2,
            (segment_AR.p1.worldPosition.z + segment_AR.p2.worldPosition.z) / 2)
        this.worldPosition = midPosition ;

        //Orthogonal direction
        val difference = Vector3.subtract(
            segment_AR.p1.worldPosition,
            segment_AR.p2.worldPosition
        ).normalized()
        val q = Quaternion.multiply(
            Quaternion.lookRotation(difference.normalized(),Vector3.forward()),
            Quaternion(Vector3.up(), 90F)
        )
        previusPoint1WorldPosition = segment_AR.p1.worldPosition
        previusPoint2WorldPosition = segment_AR.p2.worldPosition
        this.worldRotation = q
    }

    }

