package com.esieeit.arcramif.aritem

import android.content.Context
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.ux.TransformableNode
import com.google.ar.sceneform.ux.TransformationSystem

/**
 * Class Point
 * A transformable node use as a base for other aritem
 *@param context
 * @param anchorNode the anchor of the hit result
 * @param renderPoint the renderable of the point
 */
class Point(private val context: Context, val anchorNode: AnchorNode, val renderPoint: ModelRenderable,
            transformationSystem: TransformationSystem?
) :
    TransformableNode(transformationSystem) {

    /**
     * First method call, create the node and configure it
     */
    override fun onActivate() {
        if (scene == null) {
            throw IllegalStateException("Scene is null!");
        }
        this.rotationController.isEnabled = false
        this.scaleController.isEnabled = false
        renderPoint.isShadowCaster=false
        renderPoint.isShadowReceiver=false
        this.renderable = renderPoint;

//        this.setParent(anchorNode)
    }
}