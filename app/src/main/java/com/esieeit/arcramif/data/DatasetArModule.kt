package com.esieeit.arcramif.data

import android.graphics.Color

/**
 * Class like Entity of data for step collection
 */
class DatasetArModule
    (data: Any) {
    //Données en brut en attendant BDD
    val steps = ArrayList<Step>();
    val segmentNum : Int = 2;
    val zoneNum : Int = 2;

    init {
        if(data ==0) {
            steps.add(Step(0, "segment", "horizontal", value = StepValue("", 0, 0, 0, "")))
            steps.add(Step(1, "segment", "horizontal", value = StepValue("", 0, 0, 0, "")))
            steps.add(Step(2, "segment", "horizontal", value = StepValue("", 0, 0, 0, "")))
        }
        if(data ==1) {
            val value1 = StepValue("demi-cercle", 415, 1170, 0, "#80E1238C")
            steps.add(Step(0, "segment", "horizontal", value = StepValue("", 0, 0, 0, "")))
            steps.add(Step(1, "zone", "horizontal", value = value1))
        }
        if(data ==2) {
            val value1 = StepValue("demi-cercle", 415, 1170, 0, "#80E1238C")
            val value2 = StepValue("demi-cercle", 170, 480, 0, "#50319F66")
            steps.add(Step(0, "segment", "horizontal", value = StepValue("", 0, 0, 0, "")))
            steps.add(Step(1, "zone", "horizontal", value = value1))
            steps.add(Step(2, "zone", "horizontal", value = value2))
        }
        if(data ==3) {
            val value1 = StepValue("demi-cercle", 415, 1170, 0, "#80E1238C")
            val value2 = StepValue("demi-cercle", 170, 480, 1, "#80319F66")
            steps.add(Step(0, "segment", "horizontal", value = StepValue("", 0, 0, 0, "")))
            steps.add(Step(1, "zone", "horizontal", value = value1))
            steps.add(Step(2, "segment", "horizontal", value = StepValue("", 0, 0, 0, "")))
            steps.add(Step(3, "zone", "horizontal", value = value2))
            steps.add(Step(4, "segment", "horizontal", value = StepValue("", 0, 0, 0, "")))
            steps.add(Step(5, "segment", "horizontal", value = StepValue("", 0, 0, 0, "")))
        }
    }
}