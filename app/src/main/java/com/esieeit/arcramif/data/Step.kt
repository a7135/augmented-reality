package com.esieeit.arcramif.data

import org.json.JSONArray

/**
 * Class of Step entity
 */
class Step(val id: Int,val type:String,val orientation:String,val value: StepValue) {}

