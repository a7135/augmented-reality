package com.esieeit.arcramif.data

import android.graphics.Color

/**
 * Class of StepValue Entity
 */
class StepValue(val style:String,val  heigth:Int,val width:Int,val stepBase:Int,val color: String) {
}