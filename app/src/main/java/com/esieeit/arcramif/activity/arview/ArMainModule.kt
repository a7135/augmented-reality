package com.esieeit.arcramif.activity.arview

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.*
import com.esieeit.arcramif.R
import com.esieeit.arcramif.aritem.ConfortZone
import com.esieeit.arcramif.aritem.Point
import com.esieeit.arcramif.aritem.Segment_AR
import com.esieeit.arcramif.data.Constants
import com.esieeit.arcramif.data.DatasetArModule
import com.google.ar.core.*
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.FrameTime
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.*
import com.google.ar.sceneform.ux.ArFragment
import com.google.ar.sceneform.rendering.Color as arColor

/**
 * ArMainModule est notre module Ar qui cherche à etre dynamique,
 * et suit les steps d'item à ajouter en fonction des données qui lui sont transmisent
 */
class ArMainModule : ArActivity() {

    private var arFragment: ArFragment? = null

    //Interface Graphique
    private var modeTextView: TextView? = null
    lateinit var modeSpinner: Spinner
    private var modeSelected: Int = 0
    private val modeArrayList = ArrayList<String>()
    private lateinit var clearButton: Button

    //Renderable
    private var renderPoint: ModelRenderable? = null
    private var renderCardView: ViewRenderable? = null

    //Item existing in world
    private val placedAnchorNodes = ArrayList<AnchorNode>()
    private val placedOnPlane = ArrayList<Plane>()
    private val placedPoints = ArrayList<Point>()
    private val placedSegmentsAR = ArrayList<Segment_AR>();

    //Data / Steps
    private var currentStep: Int = 0
    private val data: ArrayList<DatasetArModule> = ArrayList();

    /**
     *
     * This method is the first one called when ArMainModule is instantiated
     * it will generate the view xml, load the dataSet for the step
     * and add the OnTapListener
     * @param savedInstanceState param given by the ArFragment
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_measurement)
        arFragment = supportFragmentManager.findFragmentById(R.id.sceneform_fragment) as ArFragment?
        //View
        modeTextView = findViewById(R.id.distance_view)
        val modeArrayString = resources.getStringArray(R.array.distance_mode)
        modeArrayString.map{it->
            modeArrayList.add(it)
        }
        clearButton()
        configureSpinner()
        //Renderable
        initRenderable()
        //Load Data
        data.add(DatasetArModule(0))
        data.add(DatasetArModule(1))
        data.add(DatasetArModule(2))
        data.add(DatasetArModule(3))
        //Add listener
        arFragment!!.setOnTapArPlaneListener { hitResult: HitResult, plane: Plane?, motionEvent: MotionEvent? ->
            if (plane != null) {
                oneTapListener(hitResult, plane)
            }
        }
    }

    /**
     *This method creates graphical models for the point render shown in the scene for segment.
     *
     */
    private fun initRenderable() {
        MaterialFactory.makeTransparentWithColor(
            this,
            arColor(Color.RED)
        )
            .thenAccept { material: Material? ->
                renderPoint = ShapeFactory.makeSphere(
                    0.02f,
                    Vector3.zero(),
                    material
                )
                renderPoint!!.setShadowCaster(false)
                renderPoint!!.setShadowReceiver(false)
            }
            .exceptionally {
                val builder = AlertDialog.Builder(this)
                println(it.message)
                return@exceptionally null
            }
    }

    /**
    Gets the clear button from activity_measurement.xml and sets a listener to catch the
    signal emited when the button is clicked. If it is, it clear all the anchors present in the scene
     */
    private fun clearButton() {
        clearButton = findViewById(R.id.clearButton)
        clearButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                clearAllAnchors()
                currentStep = 0
            }
        })
    }

    /**
    Clear all anchors in the scene. This means placedAnchors (Theorical points were was the hitResult),
    placedAnchorNodes (Physical points with the attached view),
    and all arItem Created
     */
    private fun clearAllAnchors() {
        for (anchorNode in placedAnchorNodes) {
            arFragment!!.arSceneView.scene.removeChild(anchorNode)
            anchorNode.isEnabled = false
            anchorNode.anchor!!.detach()
            anchorNode.setParent(null)
        }
        placedPoints.clear()
        placedAnchorNodes.clear()
        for (i in placedSegmentsAR) i.deleteAnchor()
        placedSegmentsAR.clear()
        currentStep = 0
    }


    /**
        This method creates a node in the scene at the hitResult position.
        This function :
            - creates an ARCore anchor where the hitResult was and add it to the list of created anchors
            - creates an anchorNode and attaches it to the anchor. Then, it sets the anchorNode
            parent to the scene in order to attach it and finally, it adds the anchorNode to the list
            - creates a transformableNode (Node that can be moved/rotated/scaled), sets the renderable (2D/3D representation)
             and sets it's parent to anchorNode
        To sum up, the hierarchy of our scene is :
            ARScene < Anchor < AnchorNode < TransformableNode

        What is still to be understood :
            l.543 : arFragment!!.arSceneView.scene.addOnUpdateListener(this)
                I understood it sets the updateListener to call the function Update but when does it do it ?
                When it has been created ? When it is moved ?
                The thing that has to be understood is what are the triggers of this method "OnUpdateListener"
    Method used for the "Tap distance of 2 points" mode depending on the number of placed anchorNodes
         *
         * @param hitResult give by the event handler it's the event hit on the arScene
         * @param plane give by the event it's the plane touch by the hit
     */
    private fun oneTapListener(hitResult: HitResult, plane: Plane) {
        if (data[modeSelected].steps.size <= currentStep) {
            //Fin des steps on ne place plus rien
            return
        }
        /*
         * If the current step attempt to place a segment, the on tap listener will test
         * the number of points placed (2 points for 1 segement)
         */
        if (data[modeSelected].steps[currentStep].type == "segment") {
            val anchor = hitResult.createAnchor()
            val anchorNode = AnchorNode(anchor).apply {
                isSmoothed = true
                setParent(arFragment!!.arSceneView.scene)
            }
            val point =
                renderPoint?.let { Point(this, anchorNode, it, arFragment!!.transformationSystem) }
            point!!.setParent(anchorNode)
            placedAnchorNodes.add(anchorNode)
            placedOnPlane.add(plane)
            placedPoints.add(point)
            /*
             * If their is 2 points placed then we create our ArItem Segment
             * and we sum +1 to the current step
             */
            if (placedPoints.size > 0 && placedPoints.size % 2 == 0) {
                val s: Segment_AR? = arFragment?.let {
                    Segment_AR(
                        this,
                        it,
                        placedPoints[placedPoints.size - 2],
                        placedPoints[placedPoints.size - 1],
                        arFragment!!.transformationSystem
                    )
                }
                if (s != null) {
                    placedSegmentsAR.add(s);
                    s.setParent(arFragment!!.arSceneView.scene)
                }
                arFragment!!.arSceneView.scene.addOnUpdateListener(this)
                arFragment!!.arSceneView.scene.addChild(anchorNode)
                currentStep += 1
            }
        }
    }

    /**
     * Method that will create a ArItem ConfortZone
     */
    fun createConfortZone() {
        val stepValue = data[modeSelected].steps[currentStep].value
        val confortZone: ConfortZone? =
            arFragment?.let {
                ConfortZone(
                    this, stepValue,
                    it
                )
            };
        if (stepValue.stepBase >= 0) {
            if (confortZone != null) {
                if(stepValue.stepBase!=-1) {
                    val anchorNode = confortZone.setSegmentAnchors(
                        placedSegmentsAR[stepValue.stepBase])
                    placedAnchorNodes.add(anchorNode)
                }
                else {
                    /*
                     * Make a other add mode, for example : the confort zone without segment base
                     */
                }
            };
        }
    }


    /**
     * Method called by the OnUpdateListener signal (each frame)
     * Don't know how to ugrade performance but this method can
     * be the cause of leak of performance
     * @param frameTime give by the lifeCycle of the ArFrament
     */
    @SuppressLint("SetTextI18n")
    override fun onUpdate(frameTime: FrameTime) {
        if (data[modeSelected].steps.size <= currentStep) {
           //Fin des step
            return
        }
        else {
            checkNextStep();
        }
        //println("Update")
    }

    /**
     * Function to controle the order of the steps and configure the actions for the next step
     */
    private fun checkNextStep() {
        if (data[modeSelected].steps.size <= currentStep) {
            println("FIN DES STEPS")
            println(currentStep)
            return
        }
        if (data[modeSelected].steps[currentStep].type == "segment") {
            if (placedPoints.size == 0 || placedPoints.size % 2 != 0) {
                return
            }
        }
        if (data[modeSelected].steps[currentStep].type == "zone") {
            createConfortZone()
            currentStep += 1
            return
        }
        return
    }

    /**
     *    Configure the multiselect at the bottom of the Arview
     */
    private fun configureSpinner() {
        modeSelected = 0
        modeSpinner = findViewById(R.id.distance_mode_spinner)
        val distanceModeAdapter = ArrayAdapter(
            applicationContext,
            android.R.layout.simple_spinner_item,
            modeArrayList
        )
        distanceModeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        modeSpinner.adapter = distanceModeAdapter
        modeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val spinnerParent = parent as Spinner
                modeSelected = spinnerParent.selectedItemPosition
                clearAllAnchors()
                setMode()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                clearAllAnchors()
                setMode()
            }
        }
    }

    /**
     * Set the the text of the mode selected
     */
    private fun setMode() {
        modeTextView!!.text = modeArrayList[modeSelected]
    }
}