package com.esieeit.arcramif.activity.fragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.esieeit.arcramif.R
import com.esieeit.arcramif.activity.arview.ArMainModule
import kotlinx.android.synthetic.main.home_fragment.*

/**
 * This fragment contains the contact page
 */
class HomeFragment: Fragment(R.layout.home_fragment) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Here we set listeners to the 2 buttons displayed in the fragment to make redirections
        segment_layout.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                val intent = Intent(activity, ArMainModule::class.java)
                startActivity(intent)
            }
        })
        confort_layout.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                // the intent we
            }
        })
    }

}