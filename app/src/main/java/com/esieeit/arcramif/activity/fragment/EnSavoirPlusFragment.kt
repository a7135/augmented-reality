package com.esieeit.arcramif.activity.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.esieeit.arcramif.R

/**
 * This fragment contains the en savoir plus page
 */
class EnSavoirPlusFragment : Fragment(R.layout.fragment_en_savoir_plus) {
}