package com.esieeit.arcramif.activity

import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.esieeit.arcramif.R
import kotlinx.android.synthetic.main.main_activity_layout.*
import java.util.*

/**
 * THis activity is the one used for the very first page of the app
 */
class LandingPageActivity: AppCompatActivity() {
    private val MIN_OPENGL_VERSION = 3.0
    private val TAG: String = LandingPageActivity::class.java.getSimpleName()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity_layout)
        checkIsSupportedDeviceOrFinish(this)
        page_btn.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                val intent = Intent(application, ArcoreMeasurement::class.java)
                startActivity(intent)
                finish()
            }
        })
    }

    /**
     *     Check if the device is supported and if not, finish the activity
     */
    private fun checkIsSupportedDeviceOrFinish(activity: Activity): Boolean {
        val openGlVersionString =
            (Objects.requireNonNull(activity
                .getSystemService(Context.ACTIVITY_SERVICE)) as ActivityManager)
                .deviceConfigurationInfo
                .glEsVersion
        if (openGlVersionString.toDouble() < MIN_OPENGL_VERSION) {
            Log.e(TAG, "Sceneform requires OpenGL ES ${MIN_OPENGL_VERSION} later")
            Toast.makeText(activity,
                "Sceneform requires OpenGL ES ${MIN_OPENGL_VERSION} or later",
                Toast.LENGTH_LONG)
                .show()
            activity.finish()
            return false
        }
        return true
    }
}
