package com.esieeit.arcramif.activity.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.esieeit.arcramif.R

/**
 * This fragment contains the contact page
 */
class ContactFragment : Fragment(R.layout.fragment_contact) {
    // NEED TO BE DONE : connect buttons on action performed on buttons of the view
}