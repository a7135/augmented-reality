package com.esieeit.arcramif.activity.fragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.esieeit.arcramif.R
import com.esieeit.arcramif.activity.arview.ArMainModule

/**
 * This fragment contains the galery page
 */
class GaleryFragment: Fragment(R.layout.fragment_galery) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

}