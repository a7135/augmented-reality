package com.esieeit.arcramif.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.esieeit.arcramif.R
import com.esieeit.arcramif.activity.arview.ArMainModule
import com.esieeit.arcramif.activity.fragment.*
import kotlinx.android.synthetic.main.home_fragment.*
import kotlinx.android.synthetic.main.template_burger_layout.*

/**
 * This activity is the one called after the landing page
 *  It implements interface with burger menu navigation
 */
class ArcoreMeasurement : AppCompatActivity() {
    private val buttonArrayList = ArrayList<String>()
    private lateinit var toMeasurement: Button
    lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Set the main content to burger menu
        setContentView(R.layout.template_burger_layout)

        setupBurgerMenu()

        // Set the default page fragment
        supportFragmentManager
            .beginTransaction()
            .add(R.id.page_layout, HomeFragment())
            .commit()
    }

    /**
     *     Implementation necessary to ensure that the focused item is one of the burger menu's items
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(toggle.onOptionsItemSelected(item)){
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     *     This method sets actions on the burger menu's items
     */
    private fun setupBurgerMenu(){

        toggle = ActionBarDrawerToggle(this,  burger_layout, R.string.open, R.string.close)
        burger_layout.addDrawerListener(toggle)
        toggle.syncState()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // The listener for the item selected in burger menu
        burger_menu_view.setNavigationItemSelectedListener {
            burger_layout.closeDrawer(GravityCompat.START)
            // Perform different action depending on the id of the menu button selected
            // It takes ressources from menu/buttons in burger_menu.xml
            when (it.itemId) {

                R.id.home_btn -> replaceFragment(HomeFragment())
                R.id.posture_btn -> {
                    startActivity(Intent(this, ArMainModule::class.java))
                }

                R.id.galerie_btn -> replaceFragment(GaleryFragment())
                R.id.documentation_btn -> replaceFragment(DocumentationFragment())
                R.id.enSavoirPlus_btn -> replaceFragment(EnSavoirPlusFragment())
                R.id.tutorial_btn -> replaceFragment(TutorielFragment())
                R.id.cgu_btn -> replaceFragment(CGUFragment())
                R.id.contact_btn -> replaceFragment(ContactFragment())
            }
            true
        }
    }

    /**
     *     Function called to change the displayed fragment in the view
     */
    fun replaceFragment(fragment: Fragment){
        supportFragmentManager.beginTransaction()
            .replace(R.id.page_layout, fragment)
            .commit()
    }
}
