# ARGonomy
This is a project based on the AR technology to analyse the postural constraints at payment station.
The purpose is to analyse movements performed by the employee by placing points in a virtual scene. Like this,
the solution analyse if the placed distances match with the norms.

| Home view                            | AR demo view                            |
| ------------------------------------ | ------------------------------------ |
| ![](images/home_screen.jpg) | ![](images/demo_screen.jpg) |

# Requirements

Minimum SDK version : 25

Minimum Kotlin version : 1.5.30

Minimum Gradle version : 7.1.3

Minimum OpenGL version = 3.0

JDK version : 7

In order to run the app on a mobile device,  the debug mode has to be activated on the target device. 

To do so,  follow the links below. 

Android  :

https://www.microfocus.com/documentation/silk-test/210/en/silktestworkbench-help-en/GUID-BE1EA2BA-EFF2-4B2D-8F09-4BEE0947DFB2.html

IOS :

https://help.remo.co/en/support/solutions/articles/63000251570-how-to-activate-the-iphone-debug-console-or-web-inspector-

# Supported devices

# Run
To run the solution, first you need to install Android Studio.

After that, clone the following git repository :

	https://gitlab.com/a7135/augmented-reality

Plug your mobile device in your computer.

Now, open the project on Android Studio and click on the "Run" button or press "Shift+F10"

# Debug
To make the development part easier, we recommend to run the project on debug mode.

# Gradle
Gradle is a build assistant tool integrated in Android Studio.
The build.gradle file contains all the dependancies and configurations of the project.

This is where we import the AR library sceneform that will allow us to place 3D points in the scene.
(to learn more about it, you can go on the official website: https://developers.google.com/sceneform/)
